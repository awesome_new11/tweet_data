# import playwright
from playwright.async_api import async_playwright, Page
from pathlib import Path
import asyncio
import hashlib
import glob
import os


page = None
processed_tweets = set()

tweet_ids_queue = asyncio.Queue()


def compute_sha256(text: str) -> str:
    """
    Compute SHA256 hash of the text
    """
    return hashlib.sha256(text.encode("utf-8")).hexdigest()


async def main():
    cookies = [
        {
            "name": "auth_token",
            # Twitter won't allow viewing timeline without logging in,
            # so easiest way is to use "auth_token" cookie
            # open your existing browser session -> developer tools -> storage -> cookies
            # and copy the auth_token cookie here
            "value": "xxxxxxxxxxxxxxxxxxx",
            "domain": ".twitter.com",
            "path": "/",
            "expires": 1690551801,
            "httpOnly": False,
            "secure": True,
            "sameSite": "None",
        }
    ]

    async with async_playwright() as p:
        browser = await p.chromium.launch(headless=False)
        context = await browser.new_context(
            viewport={"width": 1920, "height": 1024}, bypass_csp=True
        )
        await context.add_cookies(cookies)
        page = await context.new_page()
        username = "martenmickos"
        await page.goto(f"https://twitter.com/{username}")

        await page.locator("[aria-label^=Timeline]").first.wait_for()

        # click accept cookies
        await page.locator("text=accept all cookies").click()

        await context.expose_function("compute_sha256", compute_sha256)
        await context.expose_function("take_screenshot_by_id", take_screenshot_by_id)
        print(f"Injecting custom js")
        try:
            await page.evaluate(Path("inject_categorizer.js").read_text())
            while True:
                await asyncio.sleep(1)
        except Exception as e:
            print(e)
            await asyncio.sleep(10000)


async def take_screenshot_by_id(id, category, created):
    print(id, category, created)
    created = parse_datetime(created)
    node = page.locator(f"[id='{id}']:has(article)")

    # to handle missclick/change category we need to delete previous screenshot (if it exists)
    delete_file_by_pattern(id)

    await node.screenshot(path=f"screenshots/{category}_{created}_{id}.png")
    return f"success"


def delete_file_by_pattern(pattern: str):
    """
    Find and delete file that that ends with `*<pattern>.png`
    """
    files = glob.glob(f"**/*{pattern}.png")
    for file in files:
        os.remove(file)


def parse_datetime(datetime_str):
    """
    Parse datetime string in format `2022-03-14T15:49:56.000Z`
    and return it in format `2022-03-14`
    """
    return datetime_str[:10]


asyncio.run(main())
