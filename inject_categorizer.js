


(async () => {
    async function watchMutations(mutations, observer) {
        listOfNodeLists = mutations.map(mr => mr.addedNodes).flat()
        addedNodes = listOfNodeLists.map(nodelist => Array.from(nodelist)).flat()
        // only interested in nodes that are actually tweets
        addedNodes = addedNodes.filter(node => node.querySelectorAll('article') !== null && node.querySelectorAll('article').length == 1)
        if (addedNodes.length > 0) {
            console.log("addedNodes", addedNodes)
            // assign unique id attribute to each node
            ids = addedNodes.map(async node => {
                hash = await compute_sha256(node.textContent)
                node.id = hash
                attach_box(node)
                return node.id

            })
            ids = await Promise.all(ids)
            console.log("ids", ids)
        }

    }
    const observer = new MutationObserver(watchMutations);


    observer.observe(document.querySelector('main'), {
        subtree: true,
        childList: true
    });



    /**
     * Attach a box to the right of the node
     * @param {Node} node - DOM node to attach the box to
     * 
     */
    function attach_box(node) {
        s = `
        <div style="padding: 5px; cursor: pointer" onclick="take_screenshot()">ru_ua</div>
        <div style="padding: 5px; cursor: pointer" onclick="take_screenshot()">hacking</div>
        <div style="padding: 5px; cursor: pointer" onclick="take_screenshot()">startups</div>
        <div style="padding: 5px; cursor: pointer" onclick="take_screenshot()">other</div>`
        const box = document.createElement('div')
        box.innerHTML = s
        // absolutely position box to the right of the node
        box.style.position = 'absolute'
        box.style.left = '100%'
        box.style.top = '0'
        box.style.backgroundColor = 'red'
        node.appendChild(box)
    }

    async function take_screenshot() {
        const target = event.target

        // first, mark all categories as not selected, aka "red"
        Array.from(target.parentNode.children).map(node => node.style.backgroundColor = "red")

        const parentID = target.parentNode.parentNode.id
        const category = target.textContent
        const created = target.parentNode.parentNode.querySelector('time').getAttribute('datetime')
        const status = await take_screenshot_by_id(parentID, category, created)
        if (status == "success") {
            target.style.backgroundColor = 'green'
        }


    }
})()
