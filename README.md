# Tweet categorizer

This might be an instance of "spend more time for automating one-off task than it would take manually" but whatever.

This tool helps to categorize user's tweets by injecting a little box next to the tweet. Just scroll user's timeline and click the category. Screenshots of tweets are saved automatically.

![tweet categorizer](./screenshot.png)

To run categorizer : `$ python collect.py`

Tweet analysis is in `analyze.ipynb`

Screenshots are in `screenshots/`


![tweets](output.png)